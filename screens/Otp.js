import React from 'react';
import {View, Text, StyleSheet,TouchableOpacity,TextInput} from 'react-native';


const Otp =props =>{
    
    return(
        <View style={styles.screen}>
            
            <Text style={styles.headline}>Verify with OTP</Text>
            <View style={{flex: 1,flexDirection:"row",marginLeft:25}}>
            <TextInput autoFocus={true} selectionColor={'black'}  maxLength = {1} keyboardType={'numeric'}  style={styles.input}  /> 
            <TextInput  selectionColor={'black'}  maxLength = {1} keyboardType={'numeric'}  style={styles.input}  /> 
            <TextInput  selectionColor={'black'}  maxLength = {1} keyboardType={'numeric'}  style={styles.input}  /> 
            <TextInput  selectionColor={'black'}  maxLength = {1} keyboardType={'numeric'}  style={styles.input}  /> 
            </View>
       
            <TouchableOpacity
                        style={styles.forButton}
                        activeOpacity = { .5 }
                        onPress={()=>{
                            props.navigation.navigate({routeName:'Map'})
                                }}>

                       
                        <Text style={styles.forText}> Verify </Text>
                            
            </TouchableOpacity> 
           
              
        </View>
    );
                            
};


const styles = StyleSheet.create({
    headline: {
        marginTop:80,
        textAlign: 'left', 
        width:'100%',
        marginLeft:25,
        fontSize: 25,
        
        
      },
    screen:{
        flex:1,
        backgroundColor: '#fff',
        

    },
    forButton: {
        paddingTop:15,
        paddingBottom:15,
        width:'90%',
        marginBottom:20,
        backgroundColor:'#000000',
        borderRadius:5,
        marginLeft:25
       
    },
    
    input:{
        
        width:"15%",
        height:50,
        marginTop:50,
        textAlign: 'center',
        borderColor:'#EAEEF2', 
        backgroundColor:'#EBEEF0',
        borderRadius:8,
        marginRight:10,
        borderWidth:1},
    forText:{
        color:'#fff',
        paddingLeft:'40%'
    }
});

export default Otp;
