import React from 'react';
import {View,Text, StyleSheet,TouchableOpacity,} from 'react-native';
import Initial from '../components/Initial';
const images = ['https://image.freepik.com/free-vector/happy-people-shopping-online_74855-5865.jpg','https://image.freepik.com/free-vector/bank-client-ordering-credit-card-online-woman-monitor-with-signed-agreement-money-cash-flat-vector-illustration-online-bank-service_74855-8229.jpg','https://image.freepik.com/free-vector/loyalty-program-concept_74855-6543.jpg']

const Scroll =props =>{
    
    
     {
     
    
        return(

            <View style={{marginTop:200,}}>
               
    
                <Initial images={images} />
                
                <View>
                <Text style={style.headline}>Welcome to Happiness</Text>
                <Text style={style.sub}>Let's first check what we deliver to your address</Text>
                </View>


                <TouchableOpacity
                        style={style.forButton}
                        activeOpacity = { .5 }
                        onPress={()=>{
                            props.navigation.navigate({routeName:'SignUp'})
                                }} >
                
                            <Text style={{color:'#fff',paddingLeft:'35%'}}> Login/Signup </Text>
                            
                    </TouchableOpacity>
               
            </View>
            
           
        )

    }
}

const style = StyleSheet.create({
headline: {
  paddingTop:120,
  textAlign: 'center', 
  fontWeight: 'bold',
  fontSize: 16,
  marginBottom:-130
  
},
sub:{
  paddingTop:130,
  textAlign: 'center',
  color:'grey',
 marginBottom:90
  
},
forButton: {
    marginTop:-10,
    paddingTop:15,
    width:"90%",
    paddingBottom:15,
    marginLeft:30,
    marginRight:30,
     marginBottom:15,
    backgroundColor:'#000000',
    borderRadius:10
    
}
});



export default Scroll;