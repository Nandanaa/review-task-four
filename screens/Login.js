import React from 'react';
import Fetch from 'react-native-fetch';
import {ScrollView, Text, Alert, StyleSheet,TouchableOpacity,TextInput, KeyboardAvoidingView, View} from 'react-native';

const Login =props =>{
    {
     
    return(

<View style={styles.screen}>
     <Text style={styles.headline}> Get started</Text>
      <Text style={styles.sub}> Experience the happiness</Text> 
      <View style={{flex: 1,flexDirection:"row-reverse"}}>
       <TextInput autoFocus={true} selectionColor={'black'}  maxLength = {10} keyboardType={'numeric'} placeholder="Mobile Number" style={styles.input}  /> 
       </View>  

               <TouchableOpacity
                        style={styles.forButton}
                        activeOpacity = { .5 }
                        onPress={()=>{
                            props.navigation.navigate({routeName:'Verify'})
                                }}
                        >

                       
                          <Text style={styles.forText}> Continue </Text>
                            
              </TouchableOpacity> 
                  
    </View>
   
    
        );
    }
}





const styles = StyleSheet.create({
  headline: {
    marginTop:80,
    width:'100%',
    marginLeft:25,
    textAlign: 'left', 
    fontSize: 22,
   
    
    
  },
  sub:{
    width:'100%',
    marginLeft:25,
    textAlign: 'left',
    paddingTop:10,
    paddingBottom:15,
    color:'grey',
   
    
  },
  screen:{
    flex:1,
    justifyContent: 'center',
    backgroundColor: '#fff',
    alignItems: 'center',
    
    
    },

  contentContainer:{
    flex:1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'},
  
  input:{
    marginBottom:'120%',
    width:"90%",
    height:40,
    paddingLeft:10,
    marginLeft:25,
    marginTop:20,
    marginRight:30,
    borderColor:'#EAEEF2', 
    backgroundColor:'#EBEEF0',
    borderRadius:5,
    borderWidth:1},


forButton: {
  paddingTop:15,
  paddingBottom:15,
  width:'90%',
  marginBottom:20,
  backgroundColor:'#000000',
  borderRadius:5
},
forText:{
    color:'#fff',
    paddingLeft:'40%'
}
});






export default Login;




