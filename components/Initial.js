import React from 'react';
import {View,Image,ScrollView,Dimensions,Text,TouchableOpacity, StyleSheet} from 'react-native';

const {width}=Dimensions.get("window");
const height =width*0.7;


export default class Initial extends React.Component{
    state={
        active:0
    }

    change=({nativeEvent})=>{
        const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width);
        if(slide!== this.state.active){
            this.setState({active:slide});
        }
    }
    
     
     render(){
     
    
        return(
            <View style={{width,height}}>
                <ScrollView  
                    pagingEnabled 
                    horizontal 
                    onScroll = {this.change}
                    showsHorizontalScrollIndicator={false}
                    style={{width,height}} >
                {
                    this.props.images.map((image,index)=>(

                <Image
                    key={index}
                    source={{uri:image}}
                    style={{width, height, resizeMode:'cover'}}/>
                    ))
                
                }
                
                
                </ScrollView>
    
                <View style={{flexDirection:'row', position:'absolute',bottom:0,alignSelf:'center'}}>
                  {
                      this.props.images.map((i,k)=>(
                      <Text key={k} style={k==this.state.active ? style.pagingActiveText: style.pagingText}>▬▬</Text>
                      
                      ))
                  }
                  
                </View>
                
    
            </View>
            
        )

    }
}

const style = StyleSheet.create({

pagingText: {
    color:'rgba(255, 102, 0,0.2)',
    marginRight:4,
    marginBottom:-30},
pagingActiveText: {color:'rgb(255, 102, 0)',marginRight:6,marginBottom:-30}
});
