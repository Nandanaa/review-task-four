import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../screens/Login';
import Otp from '../screens/Otp';
import Map from '../screens/Map';
import Scroll from '../screens/Scroll';
const PageNavigator = createStackNavigator({
    Initial:Scroll,
    SignUp: Login,
    Verify: Otp,
    Map: Map
},

{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,

  },
  defaultNavigationOptions: {
    cardStyle: { backgroundColor: '#FFFFFF' },
  },
 }
);
export default createAppContainer(PageNavigator);











